var sum=0;
var remov=0;
function init()
{
    displaySum(sum);
}

function displaySum(how)
{
    document.getElementById('sum').innerHTML = (how).toFixed(2) + "€";
}

function setDisplay(what, mode)
{
    document.getElementById(what).style.display = mode;
}

function setText(what, text)
{
    document.getElementById(what).innerHTML = text;
}

function clearSum()
{
    sum = 0;
    displaySum(sum);    
}

function remove()
{
    remov = 1;
}

function add(price)
{
    if (remov == 1)
    {
        remov = 0;
        sum -= price;
        if (sum < 0)
            sum = 0;
    }
    else
        sum += price;
    
    displaySum(sum);    
}


window.onload = init;
